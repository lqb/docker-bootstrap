#!/bin/bash
set -e

codename="precise"
arch="amd64"
repository="lqb10/ubuntu-bootstrap"
release="12.04"

( [[ -e ${codename}-${arch} ]] || sudo debootstrap --arch=${arch} ${codename} ${codename}-${arch} &&
sudo rm -rf ${codename}-${arch}/var/lib/apt/lists/* ${codename}-${arch}/var/cache/apt/archives/* &&
(sudo tar -C ${codename}-${arch} -c . | docker import -c "CMD /bin/bash" - ${repository}:${codename}-${arch}-base | tee ${codename}-${arch}.id) &&
docker tag $(sed 's|^.*:||g' ${codename}-${arch}.id) ${repository}:${release}-${arch}-base
) >${codename}-${arch}.log 2>&1 



