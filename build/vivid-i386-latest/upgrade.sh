#!/bin/bash
set -e
set -x
# Print an identifier
echo "###### Docker Container ID / Hostname = $HOSTNAME ######"

# Override invoke-rc.d to disable service restart
mkdir /tmp/bin
echo -en "#!/bin/bash\nexit 0\n" > /tmp/bin/invoke-rc.d
chmod a+x /tmp/bin/invoke-rc.d
export PATH="/tmp/bin:$PATH"

# Upgrade
export DEBIAN_FRONTEND=noninteractive
apt-get update && apt-get -y upgrade

# Tidy up
apt-get clean
apt-get autoclean
rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*  /var/cache/apt/srcpkgcache.bin  /var/cache/apt/pkgcache.bin

rm /tmp/bin/invoke-rc.d
rmdir /tmp/bin
exit 0

