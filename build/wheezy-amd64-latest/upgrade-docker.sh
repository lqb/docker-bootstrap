#!/bin/bash
set -e

codename="wheezy"
arch="amd64"
repository="lqb10/debian-bootstrap"
release="7.0"

tmpname=$(basename $(mktemp -u -t tmp_XXXXXXXXXX))

( docker build --rm=true -t ${codename}-${arch}-tmp . &&
docker run --privileged -t --name="${tmpname}" ${codename}-${arch}-tmp /tmp/upgrade.sh | tee ${codename}-${arch}-run.log &&
container_id=$(awk '/^###### Docker Container ID \/ Hostname = .* ######/{print $8}' ${codename}-${arch}-run.log | head -1) &&
docker commit -c 'CMD ["/bin/bash"]' $container_id ${release}-${arch}-tmp | tee ${codename}-${arch}-commit.log &&
docker tag $(sed 's|^.*:||g' ${codename}-${arch}-commit.log) ${repository}:${codename}-${arch}-latest &&
docker tag $(sed 's|^.*:||g' ${codename}-${arch}-commit.log) ${repository}:${release}-${arch}-latest &&
docker rm ${tmpname} &&
docker rmi ${codename}-${arch}-tmp &&
docker rmi ${release}-${arch}-tmp 
) >${codename}-${arch}.log 2>&1


